@extends('layouts.app')

@section('content')
<div class="row">
	<h1>Skins</h1>
	@foreach($skins as $skin)
		<div class="caja-skins">
			<img src="imagenes/{{ $skin->imagen }}" width="300" height="200">
			<h2>{{ $skin->titulo }}</h2>
			<p>
				<a href="{{ $skin->link }}">{{ $skin->descripcion }}</a>
			</p>
		</div>
	@endforeach
</div>
@endsection