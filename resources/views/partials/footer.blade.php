<center>
	<p>{{ config('app.name')}} | Copyright @ {{ date('Y') }} -
		<a href="mailto:michi@hotmail.com">michi@hotmail.com</a> -
		<a href="https://web.whatsapp.com/send?phone=56912345678">WhatsApp!</a>
	</p>
</center>