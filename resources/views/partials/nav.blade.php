<ul class="nav justify-content-end">
	@if (Route::has('login'))
        @auth
        	<li class="nav-item">
            	<a class="nav-link" href="{{ url('home') }}">Home</a>
            </li>
        @endauth
	@endif
	<li class="nav-item">
		<a class="nav-link" href="{{ route('proyectos') }}">Proyectos</a>
	</li>
	<li class="nav-item">
		<a class="nav-link" href="{{ route('skins') }}">Skins</a>
	</li>
	<li class="nav-item">
		<a class="nav-link" href="{{ route('reglas') }}">Reglas</a>
	</li>
{{-- 	<li class="nav-item">
		<a class="nav-link" href="#">Tienda</a>
	</li> --}}
	@if (Route::has('login'))
		@auth
			<li class="nav-item dropdown">
			    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
			        {{ Auth::user()->name }}
			    </a>
			    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
			        <a class="dropdown-item" href="{{ route('perfil') }}">Perfil</a>
			        <a class="dropdown-item" href="{{ route('logout') }}"
			           onclick="event.preventDefault();
			                         document.getElementById('logout-form').submit();">
			            {{ __('Logout') }}
			        </a>
			        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
			            @csrf
			        </form>
			    </div>
			</li>
		@else
	        <li class="nav-item">
	            <a class="nav-link" href="{{ route('login') }}">Ingresar</a>
			</li>
            @if (Route::has('register'))
            	<li class="nav-item">
                	<a class="nav-link" href="{{ route('register') }}">Registrarse</a>
                </li>
            @endif
        @endauth
	@endif
</ul>