@extends('layouts.app')

@section('content')
<div class="row">
	<h1>Proyectos</h1>
	@foreach($proyectos as $proyecto)
		<div class="caja-proyectos">
			<img src="imagenes/{{ $proyecto->imagen }}" width="300" height="200">
			<h2>{{ $proyecto->titulo }}</h2>
			<p>
				<a href="{{ $proyecto->link }}">{{ $proyecto->descripcion }}</a>
			</p>
		</div>
	@endforeach
</div>
@endsection