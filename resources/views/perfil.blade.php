@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row d-flex justify-content-center" style="background-image: url({{ isset(auth()->user()->fondo) ? '../imagenes/usuarios/ auth()->user()->fondo' : '../imagenes/usuarios/fondo_default.jpg' }}); width: 100%; height: 100%; background-size: 100% 100%;">
        <div class="col-md-6">
                <div class="card-body">
                    <div class="xp-social-profile">
                        <div class="xp-social-profile-top">
                            <div class="row">
                                <div class="col-3">
                                    <div class="xp-social-profile-star py-3">
                                        <i class="mdi mdi-star font-24"></i>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="xp-social-profile-avatar text-center">
                                    	@if(isset(auth()->user()->img_perfil))
                                    	    <img src="../imagenes/usuarios/{{ auth()->user()->img_perfil }}" alt="user-profile" class="rounded-circle img-fluid">
                                    	@else
                                    	    <img src="../imagenes/usuarios/perfil_default.jpg" alt="user-profile" class="rounded img-fluid">
                                    	@endif
                                        <span class="xp-social-profile-live"></span>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="xp-social-profile-menu text-right py-3">
                                        <i class="mdi mdi-dots-horizontal font-24"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="xp-social-profile-middle text-center">
                            <div class="row">
                                <div class="col-12 rounded">
                                    <div class="xp-social-profile-title">
                                        <h5 class="my-1 text-primary">{{ auth()->user()->apodo }}</h5>
                                    </div>
                                    <div class="xp-social-profile-subtitle">
                                        <p class="mb-3 text-primary">{{ auth()->user()->name }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="xp-social-profile-bottom text-center">
                            <div class="row">
                                <div class="col-4">
                                    <div class="xp-social-profile-media pt-3">
                                        <h5 class="text-black my-1">
                                        	<a class="btn btn-outline-primary" href="#">Mis capturas</a>
                                        </h5>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="xp-social-profile-followers pt-3">
                                        <h5 class="text-black my-1">
                                        	<a class="btn btn-outline-primary" href="#">Mis coordenadas</a>
                                        </h5>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="xp-social-profile-following pt-3">
                                        <h5 class="text-black my-1">
                                        	<a class="btn btn-outline-primary" href="#">Mis proyectos</a>
                                        </h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection