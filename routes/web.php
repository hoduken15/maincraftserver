<?php

use Illuminate\Support\Facades\Route;

Auth::routes();

Route::get('/', function () {
    return view('home');
})->name('home');
Route::get('/home', function () {
    return view('home');
})->name('home');

Route::get('/proyectos', 'ProyectController@index')->name('proyectos');
Route::get('/skins', 'SkinController@index')->name('skins');
Route::get('/reglas', 'ReglasController@index')->name('reglas');
Route::get('/perfil', 'UserController@index')->name('perfil');

