<?php

namespace App\Http\Controllers;

use App\Reglas;
use Illuminate\Http\Request;

class ReglasController extends Controller
{
    public function index()
    {
    	$reglas = Reglas::all();
        return view('reglas.index',[
            'reglas' => $reglas,
        ]);
    }

    public function create()
    {
        return view('reglas.create',[
            'reglas' => $reglas,
        ]);
    }

    public function store(Request $request)
    {
		return view('reglas.index',[
            'reglas' => $reglas,
        ]);
    }

    public function show($id)
    {
    	$regla = Reglas::find($id);
    	return view('reglas.show',[
            'regla' => $regla,
        ]);
    }

    public function edit($id)
    {
        $reglas = Reglas::find($id);
    	return view('reglas.edit',[
            'reglas' => $reglas,
        ]);
    }

    public function update(Request $request, $id)
    {
        $reglas = Reglas::find($id);

    	return view('reglas.edit',[
            'reglas' => $reglas,
        ]);
    }

    public function destroy($id)
    {
        $reglas = Reglas::find($id);

    	return view('reglas.index',[
            'reglas' => $reglas,
        ]);
    }
}
