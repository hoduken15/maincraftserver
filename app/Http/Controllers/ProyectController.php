<?php

namespace App\Http\Controllers;

use App\Proyect;
use Illuminate\Http\Request;

class ProyectController extends Controller
{
    public function index()
    {
    	$proyectos = Proyect::all();
        return view('proyectos.index',[
            'proyectos' => $proyectos,
        ]);
    }

    public function create()
    {
        return view('proyectos.create',[
            'proyectos' => $proyectos,
        ]);
    }

    public function store(Request $request)
    {
		return view('proyectos.index',[
            'proyectos' => $proyectos,
        ]);
    }

    public function show($id)
    {
    	$proyecto = Proyect::find($id);
    	return view('proyectos.show',[
            'proyecto' => $proyecto,
        ]);
    }

    public function edit($id)
    {
        $proyecto = Proyect::find($id);
    	return view('proyectos.edit',[
            'proyecto' => $proyecto,
        ]);
    }

    public function update(Request $request, $id)
    {
        $proyecto = Proyect::find($id);

    	return view('proyectos.edit',[
            'proyecto' => $proyecto,
        ]);
    }

    public function destroy($id)
    {
        $proyecto = Proyect::find($id);

    	return view('proyectos.index',[
            'proyecto' => $proyecto,
        ]);
    }
}
