<?php

namespace App\Http\Controllers;

use App\Skin;
use Illuminate\Http\Request;

class SkinController extends Controller
{
    public function index()
    {
    	$skins = Skin::all();

        return view('skins.index',[
            'skins' => $skins,
        ]);
    }

    public function create()
    {
        return view('skins.create',[
            'skins' => $skins,
        ]);
    }

    public function store(Request $request)
    {
		return view('skins.index',[
            'skins' => $skins,
        ]);
    }

    public function show($id)
    {
    	$proyecto = Skin::find($id);
    	return view('skins.show',[
            'proyecto' => $proyecto,
        ]);
    }

    public function edit($id)
    {
        $proyecto = Skin::find($id);
    	return view('skins.edit',[
            'proyecto' => $proyecto,
        ]);
    }

    public function update(Request $request, $id)
    {
        $proyecto = Skin::find($id);

    	return view('skins.edit',[
            'proyecto' => $proyecto,
        ]);
    }

    public function destroy($id)
    {
        $proyecto = Skin::find($id);

    	return view('skins.index',[
            'proyecto' => $proyecto,
        ]);
    }
}
